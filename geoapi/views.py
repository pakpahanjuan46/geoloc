from django.shortcuts import render
import requests

GEOLOC_API = 'http://free.ipwhois.io/json/'

def home(request):
    return render_page(request, GEOLOC_API)

def get_ip(request):
    ip_address = request.GET['ip_address']
    url = GEOLOC_API + ip_address
    return render_page(request, url)
    
def render_page(request, ip_address):
    response = requests.get(ip_address)
    geodata = response.json()
    return render(request, 'home.html', {
        'ip': geodata['ip'],
        'flag': geodata['country_flag'],
        'country': geodata['country'],
        'continent': geodata['continent'],
        'timezone': geodata['timezone_name'],
        'gmt': geodata['timezone_gmt'],
        'longitude': geodata['longitude'],
        'latitude': geodata['latitude'],
        'org': geodata['org']

    })